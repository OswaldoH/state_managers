import 'package:flutter/material.dart';
import 'package:state_managers/utils/routes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'name',
      debugShowCheckedModeBanner: false,
      routes: routes,
      initialRoute: Routes.home,
    );
  }
}
