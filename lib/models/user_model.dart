class UserModel {
  final String _name;
  final int _age;
  final List<String> _professions;

  UserModel(
    this._name,
    this._age,
    this._professions,
  );

  String get name => this._name;
  int get age => this._age;
  List<String> get professions => this._professions;

  UserModel copyWith({
    String name,
    int age,
    List<String> profession,
  }) {
    return UserModel(
      _name ?? this._name,
      _age ?? this._age,
      _professions ?? this._professions,
    );
  }

  @override
  String toString() =>
      'UserModel(_name: $_name, _age: $_age, _professions: $_professions)';
}
