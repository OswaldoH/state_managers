import 'package:flutter/material.dart';
import 'package:state_managers/representation/home_screen.dart';
import 'package:state_managers/representation/page1_screen.dart';
import 'package:state_managers/representation/page2_screen.dart';

class Routes {
  static const String home = "/";
  static const String page1 = "page1";
  static const String page2 = "page2";
}

Map<String, WidgetBuilder> get routes {
  return {
    Routes.home: (_) => HomeScreen(),
    Routes.page1: (_) => Page1Screen(),
    Routes.page2: (_) => Page2Screen()
  };
}
