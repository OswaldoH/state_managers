# state_managers

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

The project has 4 branches

- singleton
- provider
- cubit
- bloc

In each branch the same example is used with a different state management
